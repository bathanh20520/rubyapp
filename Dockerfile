FROM ruby:2.7.4


RUN apt-get update -qq \
    && apt-get install -y nodejs \
    && apt-get install -y postgresql-client \
    && apt-get install -y default-mysql-client \
    && apt-get -y autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /app

COPY Gemfile /app/Gemfile

COPY Gemfile.lock /app/Gemfile.lock

RUN gem install bundler && bundle install